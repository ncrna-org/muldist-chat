## Download and compile

```sh
git clone https://gitlab.com/ncrna-org/muldist-chat
cd assets/docs-pdf/en
xelatex index.tex
makeglossaries index
bibtex index
xelatex index.tex
xelatex index.tex
```
