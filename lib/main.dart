import 'package:flutter/foundation.dart';
import 'package:get_it/get_it.dart';
import 'package:go_router/go_router.dart';
import 'package:logging/logging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/strings.dart';
import 'package:muldistchat/shared/constants.dart';
import 'package:muldistchat/shared/routes.dart';
import 'package:muldistchat/shared/themes.dart';

void main() {
  // Set logger for debug
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((record) {
    if (kDebugMode) {
      print(
        '[${record.level.name}] (${record.loggerName}) ${record.time}: ${record.message}',
      );
    }
  });
  GetIt.I.registerSingleton<Logger>(Logger('main'));
  runApp(const MainApp());
}

class MainApp extends StatefulWidget {
  const MainApp({super.key});

  @override
  State<MainApp> createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  // Router must be outside of build method so that hot reload does not reset
  // the current path.
  static final GoRouter router = GoRouter(routes: Routes.routes);

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: Constants.appName,
      theme: Themes.getThemeData(context, Brightness.light),
      darkTheme: Themes.getThemeData(context, Brightness.dark),
      localizationsDelegates: R.localizationsDelegates,
      supportedLocales: R.supportedLocales,
      routerConfig: router,
    );
  }
}
