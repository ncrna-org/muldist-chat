import 'package:flutter/material.dart';

abstract class Constants {
  // App name used only for MaterialApp title
  static const String appName = 'Muldist Chat';

  // theme colors
  static const Color primaryColor = Colors.deepPurple;
}
