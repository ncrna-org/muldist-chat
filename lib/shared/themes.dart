import 'package:flutter/material.dart';
import 'package:muldistchat/shared/constants.dart';

// Use abstract class to avoid instantiation, act as namespace
abstract class Themes {
  // get bright theme data and dark theme data
  static ThemeData getThemeData(BuildContext context, Brightness brightness) {
    return ThemeData(
      useMaterial3: true,
      brightness: brightness,
      colorScheme: ColorScheme.fromSeed(
        brightness: brightness,
        seedColor: Constants.primaryColor,
      ),
    );
  }

  static const double columnWidth = 360.0;
  static const double navRailWidth = 64.0;

  // check whether on mobile (small window of desktop) or desktop
  static bool isColumnMode(BuildContext context) =>
      MediaQuery.of(context).size.width > columnWidth * 2 + navRailWidth;
}
