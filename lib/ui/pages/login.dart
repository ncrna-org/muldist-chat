import 'package:flutter/material.dart';
import 'package:muldistchat/ui/views/login_view.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => LoginPageController();
}

class LoginPageController extends State<LoginPage> {
  @override
  Widget build(BuildContext context) => const LoginView();
}
