# Muldist Chat

A chat app that supports multiple decentralized messaging protocols such as XMPP and Matrix with a focus on security and privacy.

## Basic User Manual for XMPP and Matrix

[English](assets/docs-pdf/en/index.pdf) | [中文](assets/docs-pdf/zh/index.pdf)

In order to facilitate newcomers to get started and to consult the user manual at any time, we have developed a user manual in multiple languages and integrated them into the software.